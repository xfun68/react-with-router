import React from 'react';
import {Link} from "react-router-dom";

function AboutUs(props) {
  return (
    <div>
      <p>This is AboutUs page.</p>
      <p>View our <Link to='/'>website</Link></p>
    </div>
  );
}

export default AboutUs;