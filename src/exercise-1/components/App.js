import React, {Component} from 'react';
import '../styles/App.css';
import '../styles/reset.css';
import {BrowserRouter as Router, NavLink, Route} from 'react-router-dom';
import Home from "./Home";
import MyProfile from "./MyProfile";
import AboutUs from "./AboutUs";

class App extends Component {
  render() {
    return (
      <div className="app">
        <Router>
          <header>
            <nav>
              <ul>
                <li><NavLink exact activeClassName='selected' to="/">Home</NavLink></li>
                <li><NavLink activeClassName='selected' to="/my-profile">My Profile</NavLink></li>
                <li><NavLink activeClassName='selected' to="/about-us">About Us</NavLink></li>
              </ul>
            </nav>
          </header>

          <Route>
            <div className='content'>
              <Route exact path="/" component={Home}/>
              <Route path="/my-profile" component={MyProfile}/>
              <Route path="/about-us" component={AboutUs}/>
            </div>
          </Route>
        </Router>
      </div>
    );
  }
}

export default App;
